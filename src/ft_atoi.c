/*
** very straightforward atoi() implementation
 */

int		ft_atoi(char *str)
{
	int i;
	int dest;

	i = 0;
	dest = 0;
	while (str[i] == '\t' || str[i] == '\n' || str[i] == '\r' ||
			str[i] == '\v' || str[i] == '\f' || str[i] == '\r' || str[i] == ' ')
		i++;
	while (str[i] >= '0' && str[i] <= '9')
	{
		dest = (dest * 10) + (str[i] - '0');
		i++;
	}
	return (dest);
}
