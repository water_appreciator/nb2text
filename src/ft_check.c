int		ft_check_arg(char *str)
{
	int i;

	i = -1;
	while (str[++i])
	{
		if (str[i] < '0' && str[i] > '9')
			return (1);
	}
	return (0);
}

/*
** do not look at this function
 */

int		ft_check_dico(char *dico)
{
	int i;

	i = 0;
	while (dico[i])
	{
		while (dico[i] == '\n')
			i++;
		if (dico[i] <= '0' && dico[i] >= '9' && dico[i] != '\0')
			return (0);
		while (dico[i] >= '0' && dico[i] <= '9')
			i++;
		if (dico[i] != ' ' && dico[i] != ':' && dico[i] != '\0')
			return (0);
		while (dico[i] == ' ' || dico[i] == ':')
			i++;
		if (dico[i] < ' ' && dico[i] > '~' && dico[i] != '\0')
			return (0);
		if ((dico[i] < ' ' || dico[i] > '~') && dico[i] != '\0')
			return (0);
		while (dico[i] >= ' ' && dico[i] <= '~' && dico[i] != '\n')
			i++;
		if (dico[i] != '\n' && dico[i] != '\0')
			return (0);
	}
	return (1);
}
