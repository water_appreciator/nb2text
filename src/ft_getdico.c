#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include "../inc/ourlib.h"

/*
** this splits returns only the text part of a line
** back when this was written, this was the only way I found
** this is not particularly bad, the allocations are dirty though
 */

char	**ft_getdico(const char *str)
{
	int		i;
	int		j;
	int		a;
	int		prev;
	char	*dest;
	char	**strs_tab;

	prev = 0;
	dest = (char*)malloc(sizeof(char) * 10000);
	strs_tab = (char**)malloc(sizeof(char*) * 1000);
	i = -1;
	j = 0;
	a = 0;
	while (str[++i])
	{
		while (str[i] > ' ' && str[i] <= '~' && str[i] != ':')
		{
			dest[j] = str[i];
			j++;
			i++;
		}
		if (str[i] == '\n')
		{
			strs_tab[a] = dest + prev;
			i++;
			a++;
		}
		prev = j + 1;
		j++;
	}
	return (strs_tab);
}
